#!/usr/bin/env ruby
# frozen_string_literal: true

require 'gitlab'
require 'optparse'
require 'tabulo'

require File.expand_path('../lib/token_finder.rb', __dir__)

Options = Struct.new(
  :token,
  :hours,
  :debug
)

class FailedReviewAppDeploysReportOptionParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-t', '--token ACCESS_TOKEN', String, 'A valid access token') do |value|
        args.token = value
      end

      opts.on('--hours HOURS', Integer, 'Number of hours to look back') do |value|
        args.hours = value
      end

      opts.on('-d', '--debug', 'Print debugging information') do |value|
        args.debug = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class FailedReviewAppDeploysReport
  COM_GITLAB_API = 'https://gitlab.com/api/v4'
  GITLAB_PROJECT = 'gitlab-org/gitlab'
  REVIEW_DEPLOY_JOB_NAME = 'review-deploy'

  def initialize(options)
    @options = options
  end

  def report
    failed_jobs = []

    client.jobs(GITLAB_PROJECT, scope: 'failed', per_page: 100).auto_paginate do |job|
      break if report_time - Time.parse(job.created_at).to_i > duration
      next if job.name != REVIEW_DEPLOY_JOB_NAME

      failed_jobs << job
    end

    failed_jobs
  end

  private

  def client
    @client ||= Gitlab.client(endpoint: COM_GITLAB_API, private_token: TokenFinder.find_token!(@options.token))
  end

  def duration
    @duration ||= 3600 * @options.hours
  end

  def report_time
    @report_time ||= Time.now.to_i
  end
end

if $0 == __FILE__
  options = FailedReviewAppDeploysReportOptionParser.parse(ARGV)
  start = Time.now
  engine = FailedReviewAppDeploysReport.new(options)

  failed_jobs = engine.report

  if failed_jobs.empty?
    puts "No failed review app deploys in the past #{options.hours} hours"
    exit
  end

  table = Tabulo::Table.new(failed_jobs, :id) do |t|
    t.add_column(:name, width: 15)
    t.add_column(:created_at, width: 30)
    t.add_column(:status)
    t.add_column(:web_url, width: 60)
  end

  puts table

  puts "\nDone in #{Time.now - start} seconds." if options.debug
end
