class TokenFinder
  def self.find_token!(token, suffix: nil)
    new(token, suffix).find_token!
  end

  attr_reader :token, :suffix

  def initialize(token, suffix)
    @token = token
    @suffix = suffix
  end

  def find_token!
    find_token_from_attrs || find_token_from_env || find_token_from_file
  end

  def find_token_from_attrs
    token
  end

  def find_token_from_env
    ENV['GITLAB_API_TOKEN']
  end

  def find_token_from_file
    @token_from_file ||= File.read(token_file_path).strip
  rescue Errno::ENOENT
    $stderr.puts "Please provide a valid access token with the `-t/--token` option or in the `#{token_file_path}` file!"
    exit 1
  end

  private

  def token_file_path
    @token_file_path ||= File.expand_path("../api_token#{"_#{suffix}" if suffix}", __dir__)
  end
end
