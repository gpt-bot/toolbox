class TimeHelpers
  # Google Sheets can't handle real iso8601 timestamps
  def self.format_time(string)
    Time.parse(string).strftime('%Y-%m-%d %H:%I:%S')
  end
end
